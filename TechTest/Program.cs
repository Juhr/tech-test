﻿using System;
using System.IO;

namespace TechTest
{
    // Git repository for this program can be found at: bitbucket.org/juhr/tech-test
    //
    // Provides the console interface for the application.
    public class Program
    {
        // Messages
        private const string PROCESS_ABORTED = "Process aborted.";
        private const string CRITICAL_FAILURE = "Critical Failure: {0}";
        private const string NO_INPUT_PATH = "No input file path provided, parsing test data.";
        private const string NO_OUTPUT_PATH = "No output file path provided, printing to console.";
        private const string INPUT_FILE_READ = "Input file read from: {0}";
        private const string OUTPUT_FILE_WRITTEN = "Output written to: {0}";

        private const string TEST_DATA = "56T[zebra(foolish\\75\\toledo\\wisconsin\\56\\)horse(\\5\\alien\n\\yelling\\hello\\alien\\88\\felt\\79)]garbage[zebra(great\\44\\43\\sold\\\\)byfar(1\\3\\2\\5\\m8T)]welling";

        private Parser parser;

        static void Main(string[] args)
        {
            new Program().Run(args);
            Console.Read();
        }

        // Instantiate the program.
        public Program()
        {
            this.parser = new Parser();
        }

        // Run the program's logic.
        public void Run(string[] args)
        {
            try
            {
                // Parse the arguments
                var paths = this.parser.ParseArguments(args);

                // Read the input file path
                var input = this.ReadInput(paths[0]);

                // Parse the input
                var output = this.parser.ParseInput(input);

                // Write the output
                this.WriteOutput(paths[1], output[0]);
                this.WriteOutput(paths[2], output[1]);
                
            }
            catch (Exception e) when 
                (e is UnrecognizedOperatorException ||
                e is InputFileException ||
                e is OutputFileException ||
                e is UnexpectedSymbolException ||
                e is UnacceptableNameException ||
                e is NumericNameParseException ||
                e is ItemSeparatorException ||
                e is NoGroupException)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(PROCESS_ABORTED);
            }
            catch (Exception e)
            {
                // Unexpected exception, report critical failure
                Console.WriteLine(CRITICAL_FAILURE, e.Message);
                Console.WriteLine(PROCESS_ABORTED);
            }
        }

        // Writes the given text to the file at the given path.
        private void WriteOutput(string filePath, string text)
        {
            // Check whether we're writing to file or console
            if (string.IsNullOrEmpty(filePath))
            {
                Console.WriteLine(NO_OUTPUT_PATH);
                Console.WriteLine(text);
                return;
            }

            // Attempt to write the text to the file at the given path
            try
            {
                File.WriteAllText(filePath, text);
                Console.WriteLine(OUTPUT_FILE_WRITTEN, filePath);
            }
            catch (Exception e)
            {
                throw new OutputFileException(filePath, e);
            }
        }

        // Reads and returns the input from the input file.
        private string ReadInput(string filePath)
        {
            // Check whether we're using the test data
            if (string.IsNullOrEmpty(filePath))
            {
                Console.WriteLine(NO_INPUT_PATH);
                return TEST_DATA;
            }

            // Attempt to read the contents of the input file
            try
            {
                var input = File.ReadAllText(filePath);
                Console.WriteLine(INPUT_FILE_READ, filePath);
                return input;
            }
            catch (Exception e)
            {
                throw new InputFileException(filePath, e);
            }
        }
    }
}
