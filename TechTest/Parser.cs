﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TechTest
{
    // The parser for the program's command line arguments and input file.
    public class Parser
    {
        // Argument operators
        private const string OPERATOR_INPUT_PATH = "input";
        private const string OPERATOR_ALPHA_PATH = "alpha";
        private const string OPERATOR_NUMER_PATH = "numer";
        private const char ARGUMENT_SPLITTER = '=';

        // Input symbols
        private const char GROUPS_START = '[';
        private const char GROUPS_END = ']';
        private const char ITEMS_START = '(';
        private const char ITEMS_END = ')';
        private const char ITEM_SEPARATOR = '\\';
        private HashSet<char> inputSymbols = new HashSet<char>()
        {
            GROUPS_START,
            GROUPS_END,
            ITEMS_START,
            ITEMS_END,
            ITEM_SEPARATOR
        };

        // Input parse variables
        private StringBuilder stringBuilder = new StringBuilder();
        private string input;
        private bool endOfInput;
        private int index;
        private char character;
        private string currentGroup;
        private Dictionary<string, List<string>> alphaNumericGroups;
        private Dictionary<string, List<int>> numericGroups;

        // Parse output variables
        private StringComparer stringComparer = StringComparer.Create(System.Globalization.CultureInfo.InvariantCulture, true);

        // Parses the command line arguments into their respective file paths and returns them in a string array.
        public string[] ParseArguments(string[] args)
        {
            var paths = new string[] { string.Empty, string.Empty, string.Empty };
            var argSplitter = new char[] { ARGUMENT_SPLITTER };
            var operators = new HashSet<string>() { OPERATOR_INPUT_PATH, OPERATOR_ALPHA_PATH, OPERATOR_NUMER_PATH };
            for (int i = 0; i < args.Length; i++)
            {
                var split = args[i].Split(argSplitter, 2, StringSplitOptions.RemoveEmptyEntries);
                var op = split[0];

                // Ensure the operator is recognized
                if (!operators.Contains(op))
                {
                    throw new UnrecognizedOperatorException(op);
                }

                // Lack of file path can be handled later with default behaviour
                if (split.Length == 1)
                {
                    continue;
                }

                // Match the operator
                switch (op)
                {
                    case OPERATOR_INPUT_PATH:
                        paths[0] = split[1];
                        break;
                    case OPERATOR_ALPHA_PATH:
                        paths[1] = split[1];
                        break;
                    case OPERATOR_NUMER_PATH:
                        paths[2] = split[1];
                        break;
                }
            }

            return paths;
        }

        // Parses the input into ordered groups and values.
        public string[] ParseInput(string input)
        {
            // Set up parsing
            this.input = input;
            this.endOfInput = false;
            this.index = -1;
            this.currentGroup = string.Empty;
            this.alphaNumericGroups = new Dictionary<string, List<string>>();
            this.numericGroups = new Dictionary<string, List<int>>();

            // Begin parsing the input
            this.NextCharacter();
            while (!this.endOfInput)
            {
                if (this.character == GROUPS_START)
                {
                    // Begin parsing groups
                    this.NextCharacter();
                    this.ParseGroups();
                }
                else if (this.inputSymbols.Contains(this.character))
                {
                    // Unexpected use of symbol
                    throw new UnexpectedSymbolException(this.character);
                }
                this.NextCharacter();
            }

            // Sort and create the output strings
            return this.SortAndCreateOuput();
        }

        // Parses groups within the '[' and ']' symbols.
        private void ParseGroups()
        {
            while (true)
            {
                if (char.IsLetterOrDigit(this.character))
                {
                    // Begin parsing group name
                    this.ParseGroupName();
                }
                else if (this.character == GROUPS_END)
                {
                    // End this group parse
                    this.currentGroup = string.Empty;
                    break;
                }
                else if (this.character == ITEMS_START)
                {
                    // Begin parsing items
                    this.NextCharacter();
                    this.ParseItems();
                }
                else if (this.inputSymbols.Contains(this.character))
                {
                    // Unexpected use of symbol
                    throw new UnexpectedSymbolException(this.character);
                }
                this.NextCharacter();
            }
        }

        // Parses a list of items between the '(' and ')' symbols.
        private void ParseItems()
        {
            // Ensure we have a current group
            if (string.IsNullOrEmpty(this.currentGroup))
            {
                throw new NoGroupException();
            }

            var separated = true;
            while (true)
            {
                if (char.IsLetterOrDigit(this.character))
                {
                    // Check if we've had a separator
                    if (!separated)
                    {
                        throw new ItemSeparatorException();
                    }

                    // Begin parsing item name
                    this.ParseItemName();
                    separated = false;
                }
                else if (this.character == ITEM_SEPARATOR)
                {
                    // Doesn't matter if we've already separated
                    separated = true;
                }
                else if (this.character == ITEMS_END)
                {
                    // End this items parse
                    break;
                }
                else if (this.inputSymbols.Contains(this.character))
                {
                    // Unexpected use of symbol
                    throw new UnexpectedSymbolException(this.character);
                }
                this.NextCharacter();
            }
        }

        // Parses a group name and sets it as the current group.
        private void ParseGroupName()
        {
            var name = this.ParseName();
            this.currentGroup = name;
        }

        // Parses an item name and adds it to the current group.
        private void ParseItemName()
        {
            var name = this.ParseName();

            // Check whether alphanumeric or numeric
            var alpha = false;
            var numeric = false;
            for (int i = 0; i < name.Length; i++)
            {
                var c = name[i];
                if (char.IsLetter(c))
                {
                    // Set alpha and break (we have all the information we need)
                    alpha = true;
                    break;
                }
                else if (char.IsNumber(c))
                {
                    numeric = true;
                }
            }

            // Add as either alphenumeric or numeric
            if (alpha)
            {
                this.AddItemToGroup(name, this.alphaNumericGroups);
            }
            else if (numeric)
            {
                // Parse the name to an integer
                int integer = 0;
                if (!Int32.TryParse(name, out integer))
                {
                    // Couldn't parse the integer
                    throw new NumericNameParseException(name);
                }
                this.AddItemToGroup(integer, this.numericGroups);
            }
            else
            {
                // Name is neither alphanumeric nor numeric
                throw new UnacceptableNameException(name);
            }
        }

        // Parses either a group or an item name.
        private string ParseName()
        {
            this.stringBuilder.Clear();

            // Build the name until we encounter a non-letter or non-digit
            while (true)
            {
                this.stringBuilder.Append(this.character);

                // Check the next valid character
                var peek = this.PeekCharacter();
                if (char.IsLetterOrDigit(peek))
                {
                    this.NextCharacter();
                }
                else
                {
                    break;
                }
            }

            return this.stringBuilder.ToString();
        }

        // Finds the current group within a collection (or creates it if it doesn't exist) and adds the given item to it.
        private void AddItemToGroup<T>(T item, Dictionary<string, List<T>> groupCollection)
        {
            List<T> group = null;
            if (!groupCollection.TryGetValue(this.currentGroup, out group))
            {
                group = new List<T>();
                groupCollection.Add(this.currentGroup, group);
            }

            // Ensure no duplicates are listed
            if (!group.Contains(item))
            {
                group.Add(item);
            }
        }

        // Reads the next character in the input.
        private void NextCharacter()
        {
            this.index++;
            if (this.index >= this.input.Length)
            {
                // We have reached the end of the input
                this.endOfInput = true;
            }
            else
            {
                this.character = this.input[this.index];
            }
        }

        // Peek the next character in the input.
        private char PeekCharacter()
        {
            if (this.index + 1 >= this.input.Length)
            {
                return ' ';
            }
            else
            {
                return this.input[this.index + 1];
            }
        }

        // Sorts the groups and creates their output strings.
        private string[] SortAndCreateOuput()
        {
            var output = new string[2];

            // Begin with alphanumeric
            var orderedGroupNames = this.alphaNumericGroups.Keys.OrderBy(name => name);
            foreach (var group in this.alphaNumericGroups.Values)
            {
                group.Sort(this.stringComparer);
            }
            output[0] = this.CreateOutputString(orderedGroupNames, this.alphaNumericGroups);

            // Sort and output numeric
            orderedGroupNames = this.numericGroups.Keys.OrderBy(name => name);
            foreach (var group in this.numericGroups.Values)
            {
                group.Sort();
            }
            output[1] = this.CreateOutputString(orderedGroupNames, this.numericGroups);
            return output;
        }

        // Create the output string from the given group names and their groups.
        private string CreateOutputString<T>(IEnumerable<string> groupNames, Dictionary<string, List<T>> groups)
        {
            this.stringBuilder.Clear();

            // Write each group
            foreach (var groupName in groupNames)
            {
                this.stringBuilder.AppendLine(groupName);

                // Write each item of the group
                foreach (var item in groups[groupName])
                {
                    this.stringBuilder.AppendFormat(" {0}\n", item.ToString());
                }
                this.stringBuilder.AppendLine();
            }

            // Trim any whitespace
            return this.stringBuilder.ToString().TrimEnd(' ', '\n', '\r');
        }
    }
}
