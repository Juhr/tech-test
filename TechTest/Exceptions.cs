﻿using System;

namespace TechTest
{
    // Thrown when an unrecognized operator is encountered during argument parsing.
    public class UnrecognizedOperatorException : Exception
    {
        private const string FORMAT_MESSAGE = "Error: operator '{0}' is not recognized.";

        public UnrecognizedOperatorException(string op)
            :base(string.Format(FORMAT_MESSAGE, op))
        { }
    }

    // Thrown when an exception occurs during the reading of the input file.
    public class InputFileException : Exception
    {
        private const string FORMAT_MESSAGE = "Error: unable to read from input file '{0}'.\n{1}";

        public InputFileException(string filePath, Exception inner)
            :base(string.Format(FORMAT_MESSAGE, filePath, inner.Message), inner)
        { }
    }

    // Thrown when an exception occurs during the writing of an output file.
    public class OutputFileException : Exception
    {
        private const string FORMAT_MESSAGE = "ErrorL unable to write to output file '{0}'.\n{1}";

        public OutputFileException(string filePath, Exception inner)
            :base(string.Format(FORMAT_MESSAGE, filePath, inner.Message), inner)
        { }
    }

    // Thrown when an unexpected symbol is encountered during the parsing of input.
    public class UnexpectedSymbolException : Exception
    {
        private const string FORMAT_MESSAGE = "Error: unexpected use of symbol '{0}'.";

        public UnexpectedSymbolException(char symbol)
            :base(string.Format(FORMAT_MESSAGE, symbol))
        { }
    }

    // Thrown when a name is encountered which is neither alphanumeric nor numeric.
    public class UnacceptableNameException : Exception
    {
        private const string FORMAT_MESSAGE = "Error: name is neither alphanumeric nor numeric '{0}'";

        public UnacceptableNameException(string name)
            :base(string.Format(FORMAT_MESSAGE, name))
        { }
    }

    // Thrown when a name is parsed as numeric but is unable to be parsed from a string to an Int32.
    public class NumericNameParseException : Exception
    {
        private const string FORMAT_MESSAGE = "Error: unable to parse numeric name '{0}' to a 32bit Integer.";

        public NumericNameParseException(string numericName)
            :base(string.Format(FORMAT_MESSAGE, numericName))
        { }
    }

    // Thrown when a separator is not used between items in a group.
    public class ItemSeparatorException : Exception
    {
        private const string MESSAGE = "Error: item names must be separated with '\\'.";

        public ItemSeparatorException()
            :base(MESSAGE)
        { }
    }

    // Thrown when an item collection is given without a leading group name.
    public class NoGroupException : Exception
    {
        private const string MESSAGE = "Error: no group name for items.";

        public NoGroupException()
            :base(MESSAGE)
        { }
    }
}
